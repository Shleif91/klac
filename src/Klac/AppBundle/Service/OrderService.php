<?php

namespace Klac\AppBundle\Service;

use Klac\AppBundle\DataManager\OrderDataManager;
use Klac\AppBundle\Entity\Order;
use Symfony\Component\DependencyInjection\ContainerInterface;

class OrderService
{
    /**
     * @var OrderDataManager
     */
    protected $dataManager;

    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * OrderService constructor
     *
     * @param OrderDataManager $dataManager
     * @param ContainerInterface $container
     */
    public function __construct(OrderDataManager $dataManager, ContainerInterface $container)
    {
        $this->dataManager = $dataManager;
        $this->container = $container;
    }

    /**
     * @return array|Order[]
     */
    public function getOrders()
    {
        $orders = $this->dataManager->getAllOrders();

        return $orders;
    }

    /**
     * @return \Doctrine\ORM\Query
     */
    public function getOrdersQuery()
    {
        $orders = $this->dataManager->getAllOrdersQuery();

        return $orders;
    }

    /**
     * @param Order $order
     */
    public function saveOrder(Order $order)
    {
        $orderManager = $this->container->get('fos_user.user_manager');
        $orderManager->updateOrder($order);
    }

    /**
     * @param Order $order
     */
    public function deleteOrder(Order $order)
    {
        $this->dataManager->delete($order);
    }
}