<?php

namespace Klac\AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Order
 * @package Klac\AppBundle\Entity
 *
 * @ORM\Entity(repositoryClass="Klac\AppBundle\Repository\OrderRepository")
 * @ORM\Table(name="orders")
 */
class Order
{
    const NEW_STATUS = 0;
    const BROWSING = 1;
    const CLOSED = 2;

    /**
     * @var string
     *
     * @ORM\Column(type="guid")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="UUID")
     */
    protected $id;

    /**
     * @var Company
     *
     * @ORM\ManyToOne(targetEntity="Company", inversedBy="orders")
     * @ORM\JoinColumn(name="company_id", referencedColumnName="id", onDelete="SET NULL")
     */
    protected $company;

    /**
     * @var integer
     *
     * @ORM\Column(type="integer")
     */
    protected $numberViews;

    /**
     * @var integer
     *
     * @ORM\Column(type="integer")
     */
    protected $viewed = 0;

    /**
     * @var integer
     *
     * @ORM\Column(type="integer")
     *
     * @Assert\Choice({0, 1, 2})
     *
     * 1 - new
     * 2 - browsing
     * 3 - closed
     */
    protected $status;

    /**
     * @ORM\ManyToMany(targetEntity="Klac\AppBundle\Entity\User", inversedBy="orders")
     * @ORM\JoinTable(name="orders_users")
     */
    protected $users;

    /**
     * Order constructor
     */
    public function __construct()
    {
        $this->users = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Company
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * @param Company $company
     * @return Order
     */
    public function setCompany(Company $company)
    {
        $this->company = $company;

        return $this;
    }

    /**
     * @return int
     */
    public function getNumberViews()
    {
        return $this->numberViews;
    }

    /**
     * @param int $numberViews
     * @return Order
     */
    public function setNumberViews($numberViews)
    {
        $this->numberViews = $numberViews;

        return $this;
    }

    /**
     * @return int
     */
    public function getViewed()
    {
        return $this->viewed;
    }

    /**
     * @param int $viewed
     * @return Order
     */
    public function setViewed($viewed)
    {
        $this->viewed = $viewed;

        return $this;
    }

    /**
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param int $status
     * @return Order
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getUsers()
    {
        return $this->users;
    }

    /**
     * @param mixed $users
     * @return Order
     */
    public function setUsers($users)
    {
        $this->users = $users;

        return $this;
    }
}