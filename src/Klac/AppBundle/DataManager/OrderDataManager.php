<?php

namespace Klac\AppBundle\DataManager;

use Klac\AppBundle\Entity\Order;
use Klac\CoreBundle\DataManager\EntityDataManager;

class OrderDataManager extends EntityDataManager
{
    const ENTITY_NAME = 'KlacAppBundle:Order';

    /**
     * @return array|Order[]
     */
    public function getAllOrders()
    {
        $users = $this->repository->findAll();

        return $users;
    }

    /**
     * @return \Doctrine\ORM\Query
     */
    public function getAllOrdersQuery()
    {
        $query = $this->repository->getAllOrdersQuery();

        return $query;
    }
}