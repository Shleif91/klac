<?php

namespace Klac\AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class StatisticsController
 * @package Klac\AppBundle\Controller
 */
class StatisticsController extends Controller
{
    /**
     * @Route("/", name="statistics_get")
     */
    public function indexAction()
    {
        return $this->render('KlacAppBundle:Statistics:index.html.twig');
    }

    /**
     * @Route("/api/get-statistics")
     * @Method({"GET"})
     *
     * @param Request $request
     * @return Response
     */
    public function apiStatisticsAction(Request $request)
    {
        $users = $this->get('user.service')->getUsers();

        $serializer = $this->get('serializer');

        $jsonContent = $serializer->serialize(
            $users,
            'json',
            [
                'groups' => [
                    'brief_view'
                ]
            ]
        );

        $response = new Response($jsonContent);

        $httpRequestOrigin = $request->headers->get('origin');
        $response->headers->set('Access-Control-Allow-Origin', $httpRequestOrigin);
        $response->headers->set('Access-Control-Allow-Credentials', 'true');

        return $response;
    }
}