<?php

namespace Klac\AppBundle\Controller;

use Klac\AppBundle\Entity\Order;
use Klac\AppBundle\Form\OrderType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class OrderController
 * @package Klac\AppBundle\Controller
 *
 * @Route("/orders")
 */
class OrderController extends Controller
{
    /**
     * @Route("", name="orders_index")
     *
     * @param Request $request
     * @return Response
     */
    public function indexAction(Request $request)
    {
        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $this->get('order.service')->getOrdersQuery(),
            $request->query->getInt('page', 1),
            5
        );

        $forms = [];

        foreach ($pagination as $order) {
            $forms[$order->getId()] = $this->createDeleteForm($order)->createView();
        }

        return $this->render('KlacAppBundle:Order:index.html.twig', [
            'forms' => $forms,
            'pagination' => $pagination
        ]);
    }
    /**
     * @Route("/new", name="order_new")
     * @Method({"GET", "POST"})
     *
     * @param Request $request
     * @return Response
     */
    public function newAction(Request $request)
    {
        $order = new Order();
        $form = $this->createForm(OrderType::class, $order, [
            'action' => $this->generateUrl('order_new', ['id' => $order->getId()]),
            'validation_groups' => 'Registration'
        ]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $order = $form->getData();

            $this->get('order.service')->saveUser($order);

            return $this->redirectToRoute('orders_index');
        }

        return $this->render('KlacAppBundle:Order:new.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/{id}/edit", name="order_edit")
     *
     * @param Order $order
     * @param Request $request
     * @return Response
     */
    public function editAction(Request $request, Order $order)
    {
        $form = $this->createForm(OrderType::class, $order, [
            'action' => $this->generateUrl('order_edit', ['id' => $order->getId()])
        ]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $order = $form->getData();

            $this->get('order.service')->saveOrder($order);

            return $this->redirectToRoute('orders_index');
        }

        return $this->render('KlacAppBundle:Order:edit.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/{id}", name="order_delete")
     * @Method({"DELETE"})
     *
     * @param Order $order
     * @param Request $request
     * @return Response
     */
    public function deleteAction(Request $request, Order $order)
    {
        $form = $this->createDeleteForm($order);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->get('order.service')->deleteOrder($order);
        }

        return $this->redirectToRoute('orders_index');
    }

    /**
     * @param Order $order
     * @return \Symfony\Component\Form\Form|\Symfony\Component\Form\FormInterface
     */
    private function createDeleteForm(Order $order)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('order_delete', array('id' => $order->getId())))
            ->setMethod('DELETE')
            ->getForm();
    }
}